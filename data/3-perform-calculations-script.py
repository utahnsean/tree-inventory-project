# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 12:45:38 2019

@author: Sean
"""

#### 1. Set variables and import modules

# Set feature class name
fc_path = arcpy.GetParameterAsText(0)

# Import module: arcpy and set workspace
import arcpy

arcpy.env.overwriteOutput = True


#### 3. Calculate a species rating based on species field

# Calculate species rating with update cursor
with arcpy.da.UpdateCursor(in_table = fc_path, field_names = ['SPECIES', 'SPECIES_RATING']) as updater:
    for row in updater:
        if row[0] in ('RussianOlive'):
            row[1] = 15
        elif row[0] in ('Elm-Siberian', 'Tree-of-Heaven', 'Willow-Navajo/Globe'):
            row[1] = 35
        elif row[0] in ('Chinaberry'):
            row[1] = 40
        elif row[0] in ('Ash-European', 'Poplar-Lombardy', 'Willow', 'Willow-Weeping'):
            row[1] = 45
        elif row[0] in ('Ash-Modesto', 'Ash-Raywood', 'Catalpa-Northern', 'Catalpa-Umbrella', 'Elm', 'Elm-American', 'Maple-Autumn', 'Maple-Freeman', 'Maple-Hedge', 'Maple-Hybrid', 'Maple-Silver', 'Pawpaw', 'Peach', 'Poplar', 'Poplar-Hybrid', 'Poplar-White', 'Privet-Japanese', 'Quince', 'Willow-Narrowleaf'):
            row[1] = 50
        elif row[0] in ('Arborvitae-Oriental', 'Aspen-Quaking', 'Plum', 'Plum-Purpleleaf'):
            row[1] = 55
        elif row[0] in ('Ash', 'Boxelder', 'Cherry-Flowering', 'Cherry-Sweet', 'Cottonwood', 'Cottonwood-Eastern', 'Cottonwood-Narrowleaf', 'Locust', 'Locust-Black', 'Maple-Amur', 'Maple-Red', 'Maple-StateStreet', 'Pear-Common', 'Pear-Flowering', 'Pine-Mugo', 'Plum-Common', 'Plum-Flowering', 'Willow-Peachleaf'):
            row[1] = 60
        elif row[0] in ('Birch', 'Birch-Paper', 'Chokecherry', 'Elm-Smoothleaf', 'Juniper', 'Juniper-RockyMountain', 'Mimosa', 'Zelkova-Japanese'):
            row[1] = 65
        elif row[0] in ('Alder-Black', 'Apricot', 'Birch-River', 'Birch-Water', 'Buckeye-Ohio', 'Cottonwood-Fremont', 'Hickory', 'Mulberry', 'Mulberry-Red', 'Mulberry-White', 'Pine-Aleppo', 'Pine-Lodgepole', 'Smoketree', 'Sweetgum-American'):
            row[1] = 70
        elif row[0] in ('Alder', 'Apple', 'Arborvitae-Eastern', 'Ash-Green', 'Crabapple', 'CrapeMrytle', 'Elm-Chinese', 'Elm-Lacebark', 'Fir-Douglas', 'Goldenchain', 'Maple-Sycamore', 'Oak-Live', 'Pine-Pinyon', 'Walnut-Black', 'Walnut-English'):
            row[1] = 75
        elif row[0] in ('Ash-White', 'Austrian Pine', 'Cherry-Kwanzan', 'Chinese Flame Tree', 'Filbert', 'Filbert-Turkish', 'Goldenraintree', 'Hawthorn', 'Hawthorn-Black', 'Hawthorn-Cockspur', 'Hawthorn-English', 'Hawthorn-Lavalle', 'Hawthorn-Washington', 'Honeylocust', 'Horsechestnut-European', 'Horsechestnut-Red', 'Ironwood', 'Jujube', 'Linden', 'Linden-American', 'Linden-Littleleaf', 'Linden-Silver', 'Maple-Norway', 'Mohagany-CurleafMountain', 'Mohagany-Mountain', 'Oak-Gambel', 'Orange-Osage', 'Pagodatree-Japanese', 'Pine', 'Pine-Austrian', 'Pine-Bosnian', 'Pine-Himalayan', 'Pine-JapaneseBlack', 'Pine-Ponderosa', 'Pine-White', 'Planetree-American', 'Planetree-London', 'Spruce', 'Spruce-Blue', 'Spruce-Engelmann', 'Tuliptree', 'Yellowwood-Kentucky'):
            row[1] = 80
        elif row[0] in ('Apple-Serviceberry', 'Cedar-Incense', 'Cypress', 'Cypress-Arizona', 'Cypress-Italian', 'Cypress-Leyland', 'Fir', 'Fir-White', 'Hackberry', 'Hackberry-Common', 'Hackberry-Netleaf', 'Maple-Japanese', 'Maple-RockyMountain', 'Maple-Shantung', 'Maple-Tatarian', 'Oak', 'Oak-NorthernRed', 'Pine-Scotch', 'Pistache-Chinese', 'Pistachio', 'Serviceberry', 'Spruce-Norway', 'Spruce-Serbian'):
            row[1] = 85
        elif row[0] in ('Baldcypress', 'Beech', 'Beech-European', 'Beech-American', 'Coffeetree-Kentucky', 'FringeTree-Chinese', 'Hornbeam', 'Magnolia', 'Oak-Chinkapin', 'Oak-English', 'Pine-Bristlecone', 'Pine-Limber', 'Redbud', 'Redbud-Eastern', 'Redwood-Dawn', 'TreeLilac', 'TreeLilac-Japanese'):
            row[1] = 90
        elif row[0] in ('Cedar', 'Cedar-Atlas', 'Cedar-Deodar', 'Cedar-of-Lebanon', 'GiantSequoia', 'Ginkgo', 'Larch', 'Maple-Bigtooth', 'Maple-Paperback', 'Oak-Bur', 'Oak-SwampWhite', 'Pine-Lacebark', 'Redbud-Western'):
            row[1] = 95
        elif row[0] in ('UNKNOWN'):
            row[1] = 0
        updater.updateRow(row)
del row, updater


#### 4. Calculations for tree appraisal

# Varaibles in equation
replacement_tree_size = arcpy.GetParameter(1)
installed_tree_cost = arcpy.GetParameter(2)
unit_tree_cost = arcpy.GetParameter(3)

# Calculate tree appraisal with update cursor
# Appraisal = (Basic tree cost * Species rating * Condition rating * Location rating) / 1,000,000
with arcpy.da.UpdateCursor(in_table = fc_path, field_names = ['SPECIES_RATING', 'DIAMETER', 'ROOT_HEALTH', 'TRUNK_HEALTH', 'SCAFFOLD_BRANCHES', 'SMALL_BRANCHES', 'FOLIAGE_HEALTH', 'CONDITION_RATING', 'SITE_RATING', 'SITE_CONTRIBUTION', 'PLACEMENT_RATING', 'LOCATION_RATING', 'APPRAISED_VALUE']) as updater:
    for row in updater:
       
        # Condition rating = ((Root health + Trunk health + Scaffold branches + Small branches + Foliage health)/32)*100
        row[7] = (((row[2] + row[3] + row[4] + row[5] + row[6])/32.0)*100.0)
        
        # Location rating = (Site rating + Site contribution + Placement rating)/3
        row[11] = ((row[8] + row[9] + row[10])/3.0)
        
        # Appraised value = ((Basic tree cost) * (Species rating) * (Condition rating) * (Location rating))/1,000,000
                ## Basic tree cost = ((Appraised tree trunk increase) * (Unit tree cost)) + (Installed tree cost)
                        ### Appraised tree trunk increase = (Trunk area) - (Replacement tree size trunk area)
                                #### Trunk area = (3.142 * Diameter * (Diameter/4))
                                #### Replacement tree size trunk area = (3.142 * Replacement tree size * (Replacement tree size/4))
        row[12] = (((((3.142*row[1]*(row[1]/4.0))-(3.142*replacement_tree_size*(replacement_tree_size/4.0)))*unit_tree_cost)+installed_tree_cost) * row[0] * row[7] * row[11])/1000000.0
        
        updater.updateRow(row)
del row, updater







