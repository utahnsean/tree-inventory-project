# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 12:41:13 2019

@author: Sean
"""

#### 1. Set variables and import modules

# Select an existing feature class
fc_path = arcpy.GetParameterAsText(0)

# Import module: arcpy and set workspace
import arcpy

arcpy.env.overwriteOutput = True


#### 2. Add fields

# Create a list of tuples defining the fields
fields = [("SPECIES_RATING", "SHORT", "", "", "", "Species Rating", "NULLABLE", "NON_REQUIRED", ""),
("CONDITION_RATING", "DOUBLE", "", "", "", "Condition Rating", "NULLABLE", "NON_REQUIRED", ""),
("LOCATION_RATING", "DOUBLE", "", "", "", "Location Rating", "NULLABLE", "NON_REQUIRED", ""),
("APPRAISED_VALUE", "DOUBLE", "", "", "", "Appraised Value",  "NULLABLE", "NON_REQUIRED", "")]

# Add the fields using a for loop
for field in fields:
    arcpy.AddField_management(fc_path, field[0], field[1], field[2], field[3], field[4], field[5], field[6], field[7], field[8])


#### 3. Fields added to existing shapefile

arcpy.AddMessage('Fields added to existing shapefile')



