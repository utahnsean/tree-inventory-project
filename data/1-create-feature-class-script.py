# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 12:32:03 2019

@author: Sean
"""

#### 1. Set variables and import modules

# Set working folder
data_folder = arcpy.GetParameterAsText(0)

# Set geodatabase name
gdb_name = arcpy.GetParameterAsText(1)

# Set feature class name
fc_name = arcpy.GetParameterAsText(2)

# Set alias for feature class
alias_name = arcpy.GetParameterAsText(3)

# Import modules: arcpy, os and set workspace
import arcpy
import os

arcpy.env.workspace = data_folder
arcpy.env.overwriteOutput = True


#### 2. Create geodatabase

# Create file geodatabase
arcpy.CreateFileGDB_management(data_folder, gdb_name)


#### 3. Create domains

# Create domains in geodatabase
arcpy.CreateDomain_management(gdb_name, '100by1', '100by1', field_type='Float', domain_type='Range')
arcpy.CreateDomain_management(gdb_name, '100by10', '100by10', field_type='Short', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, '100by5', '100by5', field_type='Short', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, '4by1', '4by1', field_type='Short', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, '8by1', '8by1', field_type='Short', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'comments', 'comments', field_type='Text', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'condition', 'condition', field_type='Text', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'maintenance', 'maintenance', field_type='Text', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'priority', 'priority', field_type='Text', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'scientific', 'scientific', field_type='Text', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'species', 'species', field_type='Text', domain_type='Coded')
arcpy.CreateDomain_management(gdb_name, 'yesno', 'yesno', field_type='Text', domain_type='Coded')


#### 4. Range domains

# Set domain ranges
arcpy.SetValueForRangeDomain_management(gdb_name, '100by1', 0, 100)


#### 5. Coded domains

# Create dictionary for domains
cnt100by10Dict = {'0':'0', '10':'10', '20':'20', '30':'30', '40':'40', '50':'50', '60':'60', '70':'70', '80':'80', '90':'90', '100':'100'}
cnt100by5Dict = {'0':'0', '5':'5', '10':'10', '15':'15', '20':'20', '25':'25', '30':'30', '35':'35', '40':'40', '45':'45', '50':'50', '55':'55', '60':'60', '65':'65', '70':'70', '75':'75', '80':'80', '85':'85', '90':'90', '95':'95', '100':'100'}
cnt4by1Dict = {'0':'0', '1':'1', '2':'2', '3':'3', '4':'4'}
cnt8by1Dict = {'0':'0', '1':'1', '2':'2', '3':'3', '4':'4', '5':'5', '6':'6', '7':'7', '8':'8'}
commDict = {'Canopy die-back':'Canopy die-back', 'Chlorosis':'Chlorosis', 'Co-dominant stems':'Co-dominant stems', 'Girdling root':'Girdling root', 'Included bark':'Included bark', 'Insect/disease/decline':'Insect/disease/decline', 'Irrigation issue':'Irrigation issue', 'Mechanical damage':'Mechanical damage', 'Multi-stem':'Multi-stem', 'Power line conflict':'Power line conflict', 'Root damage':'Root damage', 'Sidewalk damage':'Sidewalk damage', 'Staked':'Staked', 'Storm damage':'Storm damage', 'Stump':'Stump', 'Suckers/watersprouts':'Suckers/watersprouts', 'Topped':'Topped', 'Trunk wound':'Trunk wound', 'Vandalism':'Vandalism', 'Wildlife damage':'Wildlife damage'}
condDict = {'Dead':'Dead', 'Excellent':'Excellent', 'Fair':'Fair', 'Good':'Good', 'Poor':'Poor', 'Missing':'Missing', 'Stump':'Stump'}
mainDict = {'Create tree ring':'Create tree ring', 'Hazard limb':'Hazard limb', 'Hazard tree':'Hazard tree', 'Prune - crown raise':'Prune - crown raise', 'Prune - crown clean':'Prune - crown clean', 'Prune - crown restoration':'Prune - crown restoration', 'Prune - safety':'Prune - safety', 'Prune - structural':'Prune - structural', 'Prune - side trim':'Prune - side trim', 'Prune - building':'Prune - building', 'Re-inspect':'Re-inspect', 'Rec. removal':'Rec. removal', 'Stake removal':'Stake removal', 'Treate insect/disease':'Treate insect/disease'}
prioDict = {'Critical - public safety':'Critical - public safety', 'Immediate large tree':'Immediate large tree', 'Immediate small tree':'Immediate small tree', 'Routine large tree':'Routine large tree', 'Routine small tree':'Routine small tree'}
scienDict = {'Alnus spp.':'Alnus spp.', 'Alnus glutinosa':'Alnus glutinosa', 'Malus spp.':'Malus spp.', 'Amelanchier grandiflora':'Amelanchier grandiflora', 'Prunus armeniaca':'Prunus armeniaca', 'Thuja occidentalis':'Thuja occidentalis', 'Thuja orientalis':'Thuja orientalis', 'Fraxinus spp.':'Fraxinus spp.', 'Fraxinus excelsior':'Fraxinus excelsior', 'Fraxinus pennsylvanica':'Fraxinus pennsylvanica', 'Fraxinus velutina':'Fraxinus velutina', 'Fraxinus oxycarpa':'Fraxinus oxycarpa', 'Fraxinus americana':'Fraxinus americana', 'Populus tremuloides':'Populus tremuloides', 'Pinus nigra':'Pinus nigra', 'Taxodium distichum':'Taxodium distichum', 'Fagus spp.':'Fagus spp.', 'Fagus sylvantica':'Fagus sylvantica', 'Fagus grandifolia':'Fagus grandifolia', 'Betula spp.':'Betula spp.', 'Betula papyrifera':'Betula papyrifera', 'Betula nigra':'Betula nigra', 'Betula occidentalis':'Betula occidentalis', 'Acer negundo':'Acer negundo', 'Aesculus glabra':'Aesculus glabra', 'Catalpa speciosa':'Catalpa speciosa', 'Catalpa bignoides':'Catalpa bignoides', 'Cedrus spp.':'Cedrus spp.', 'Cedrus atlantica':'Cedrus atlantica', 'Cedrus deodara':'Cedrus deodara', 'Calocedrus decurrens':'Calocedrus decurrens', 'Cedrus libani':'Cedrus libani', 'Prunus spp.':'Prunus spp.', 'Prunus serrulata':'Prunus serrulata', 'Prunus avium':'Prunus avium', 'Melia azedarach':'Melia azedarach', 'Koelreuteria bipinnata':'Koelreuteria bipinnata', 'Prunis virginiana':'Prunis virginiana', 'Gymnocladus dioicus':'Gymnocladus dioicus', 'Populus spp.':'Populus spp.', 'Populus deltoides':'Populus deltoides', 'Populus fremontii':'Populus fremontii', 'Populus angustifolia':'Populus angustifolia', 'Malus spp.':'Malus spp.', 'Lagerstroemia indica':'Lagerstroemia indica', 'Cupressus spp.':'Cupressus spp.', 'Cupressus arizonica':'Cupressus arizonica', 'Cupressus sempervirens':'Cupressus sempervirens', 'Cupressus × leylandii':'Cupressus × leylandii', 'Ulmus spp.':'Ulmus spp.', 'Ulmus americana':'Ulmus americana', 'Ulmus parvifolia':'Ulmus parvifolia', 'Ulmus parvifolia':'Ulmus parvifolia', 'Ulmus pumila':'Ulmus pumila', 'Ulmus procera':'Ulmus procera', 'Corylus spp.':'Corylus spp.', 'Corylus colurna':'Corylus colurna', 'Abies spp.':'Abies spp.', 'Pseudotsuga menziesii':'Pseudotsuga menziesii', 'Abies concolor':'Abies concolor', 'Chionanthus retusus':'Chionanthus retusus', 'Sequoiadendron giganteum':'Sequoiadendron giganteum', 'Ginkgo biloba':'Ginkgo biloba', 'Laburnum watereri':'Laburnum watereri', 'Koelreuteria paniculata':'Koelreuteria paniculata', 'Celtis occidentalis':'Celtis occidentalis', 'Celtis occidentalis':'Celtis occidentalis', 'Celtis reticulata':'Celtis reticulata', 'Crataegus':'Crataegus', 'Crataegus douglasii':'Crataegus douglasii', 'Crataegus crus-galli':'Crataegus crus-galli', 'Crataegus laevigata':'Crataegus laevigata', 'Crataegus lavallei':'Crataegus lavallei', 'Crataegus phaenopyrum':'Crataegus phaenopyrum', 'Carya spp.':'Carya spp.', 'Gleditsia triacanthos':'Gleditsia triacanthos', 'Carpinus spp.':'Carpinus spp.', 'Aesculus hippocastanum':'Aesculus hippocastanum', 'Aesculus carnea':'Aesculus carnea', 'Casuarina spp.':'Casuarina spp.', 'Ziziphus jujuba':'Ziziphus jujuba', 'Juniperus spp.':'Juniperus spp.', 'Juniperus scopulorum':'Juniperus scopulorum', 'Larix spp.':'Larix spp.', 'Tilia spp.':'Tilia spp.', 'Tilia americana':'Tilia americana', 'Tilia cordata':'Tilia cordata', 'Tilia tomentosa':'Tilia tomentosa', 'Robinia spp.':'Robinia spp.', 'Robinia pseudoacacia':'Robinia pseudoacacia', 'Magnolia spp.':'Magnolia spp.', 'Acer ginnala':'Acer ginnala', 'Acer x. fremanii':'Acer x. fremanii', 'Acer grandidentatum':'Acer grandidentatum', 'Acer x fremanii':'Acer x fremanii', 'Acer campestre':'Acer campestre ', 'Acer spp.':'Acer spp.', 'Acer palmatum':'Acer palmatum', 'Acer platanoides':'Acer platanoides', 'Acer griseum':'Acer griseum', 'Acer rubrum':'Acer rubrum', 'Acer glabrum':'Acer glabrum', 'Acer truncatum':'Acer truncatum', 'Acer saccharinum':'Acer saccharinum', 'Acer miyabei':'Acer miyabei', 'Acer pseudoplatanus':'Acer pseudoplatanus', 'Acer tataricum':'Acer tataricum', 'Albizia julibrissin':'Albizia julibrissin', 'Cercocarpus ledifolius':'Cercocarpus ledifolius', 'Cercocarpus montanus':'Cercocarpus montanus', 'Morus spp.':'Morus spp.', 'Morus spp.':'Morus spp.', 'Morus spp.':'Morus spp.', 'Quercus spp.':'Quercus spp.', 'Quercus macrocarpa':'Quercus macrocarpa', 'Quercus muehlenbergii':'Quercus muehlenbergii', 'Quercus robur':'Quercus robur', 'Quercus gambelii':'Quercus gambelii', 'Quercus turbinella':'Quercus turbinella', 'Quercus rubra':'Quercus rubra', 'Quercus bicolor':'Quercus bicolor', 'Maclura pomifera':'Maclura pomifera', 'Sophora japonica':'Sophora japonica', 'Asimina triloba':'Asimina triloba', 'Prunus persica':'Prunus persica', 'Pyrus spp.':'Pyrus spp.', 'Pyrus calleryana':'Pyrus calleryana', 'Pinus spp.':'Pinus spp.', 'Pinus halepensis':'Pinus halepensis', 'Pinus nigra':'Pinus nigra', 'Pinus heldreichii':'Pinus heldreichii', 'Pinus longaeva':'Pinus longaeva', 'Pinus contorta':'Pinus contorta', 'Pinus wallichiana':'Pinus wallichiana', 'Pinus thunbergiana':'Pinus thunbergiana', 'Pinus bungeana':'Pinus bungeana', 'Pinus flexilis':'Pinus flexilis', 'Pinus mugo':'Pinus mugo', 'Pinus edulis':'Pinus edulis', 'Pinus ponderosa':'Pinus ponderosa', 'Pinus sylvestris':'Pinus sylvestris', 'Pinus monticola':'Pinus monticola', 'Pistacia chinensis':'Pistacia chinensis', 'Pistacia vera':'Pistacia vera', 'Platanus occidentalis':'Platanus occidentalis', 'Platanus x acerifolia':'Platanus x acerifolia', 'Prunus spp.':'Prunus spp.', 'Prunus domestica':'Prunus domestica', 'Prunus mume':'Prunus mume', 'Prunus cerasifera':'Prunus cerasifera', 'Populus spp.':'Populus spp.', 'Populus deltoids x Populus nigra':'Populus deltoids x Populus nigra', 'Populus nigra':'Populus nigra', 'Populus alba':'Populus alba', 'Ligustrum japonicum':'Ligustrum japonicum', 'Cydonia oblonga':'Cydonia oblonga', 'Cercis spp.':'Cercis spp.', 'Cercis canadensis':'Cercis canadensis', 'Cercis occidentalis':'Cercis occidentalis', 'Metesequoia glyptostroboides':'Metesequoia glyptostroboides', 'Elaeagnus angustifolia':'Elaeagnus angustifolia', 'Amalanchier spp.':'Amalanchier spp.', 'Cotinus spp.':'Cotinus spp.', 'Picea spp.':'Picea spp.', 'Picea pungens':'Picea pungens', 'Picea engelmannii':'Picea engelmannii', 'Picea abies':'Picea abies', 'Picea omorika':'Picea omorika', 'Liquidambar styraciflua':'Liquidambar styraciflua', 'Ailanthus altissima':'Ailanthus altissima', 'Syringia spp.':'Syringia spp.', 'Syringia reticulata':'Syringia reticulata', 'Liriodendron tulipifera':'Liriodendron tulipifera', 'Juglans nigra':'Juglans nigra', 'Juglans cinerea':'Juglans cinerea', 'Salix spp.':'Salix spp.', 'Salix nigra':'Salix nigra', 'Salix matsudana':'Salix matsudana', 'Salix amygdaloides':'Salix amygdaloides', 'Salix babylonica':'Salix babylonica', 'Cladrastis lutea':'Cladrastis lutea', 'Zelkova serrata':'Zelkova serrata'}
specDict = {'Alder':'Alder', 'Alder-Black':'Alder-Black', 'Apple':'Apple', 'Apple-Serviceberry':'Apple-Serviceberry', 'Apricot':'Apricot', 'Arborvitae-Eastern':'Arborvitae-Eastern', 'Arborvitae-Oriental':'Arborvitae-Oriental', 'Ash':'Ash', 'Ash-European':'Ash-European', 'Ash-Green':'Ash-Green', 'Ash-Modesto':'Ash-Modesto', 'Ash-Raywood':'Ash-Raywood', 'Ash-White':'Ash-White', 'Aspen-Quaking':'Aspen-Quaking', 'Austrian Pine':'Austrian Pine', 'Baldcypress':'Baldcypress', 'Beech':'Beech', 'Beech-European':'Beech-European', 'Beech-American':'Beech-American', 'Birch':'Birch', 'Birch-Paper':'Birch-Paper', 'Birch-River':'Birch-River', 'Birch-Water':'Birch-Water', 'Boxelder':'Boxelder', 'Buckeye-Ohio':'Buckeye-Ohio', 'Catalpa-Northern':'Catalpa-Northern', 'Catalpa-Umbrella':'Catalpa-Umbrella', 'Cedar':'Cedar', 'Cedar-Atlas':'Cedar-Atlas', 'Cedar-Deodar':'Cedar-Deodar', 'Cedar-Incense':'Cedar-Incense', 'Cedar-of-Lebanon':'Cedar-of-Lebanon', 'Cherry-Flowering':'Cherry-Flowering', 'Cherry-Kwanzan':'Cherry-Kwanzan', 'Cherry-Sweet':'Cherry-Sweet', 'Chinaberry':'Chinaberry', 'Chinese Flame Tree':'Chinese Flame Tree', 'Chokecherry':'Chokecherry', 'Coffeetree-Kentucky':'Coffeetree-Kentucky', 'Cottonwood':'Cottonwood', 'Cottonwood-Eastern':'Cottonwood-Eastern', 'Cottonwood-Fremont':'Cottonwood-Fremont', 'Cottonwood-Narrowleaf':'Cottonwood-Narrowleaf', 'Crabapple':'Crabapple', 'CrapeMrytle':'CrapeMrytle', 'Cypress':'Cypress', 'Cypress-Arizona':'Cypress-Arizona', 'Cypress-Italian':'Cypress-Italian', 'Cypress-Leyland':'Cypress-Leyland', 'Elm':'Elm', 'Elm-American':'Elm-American', 'Elm-Chinese':'Elm-Chinese', 'Elm-Lacebark':'Elm-Lacebark', 'Elm-Siberian':'Elm-Siberian', 'Elm-Smoothleaf':'Elm-Smoothleaf', 'Filbert':'Filbert', 'Filbert-Turkish':'Filbert-Turkish', 'Fir':'Fir', 'Fir-Douglas':'Fir-Douglas', 'Fir-White':'Fir-White', 'FringeTree-Chinese':'FringeTree-Chinese', 'GiantSequoia':'GiantSequoia', 'Ginkgo':'Ginkgo', 'Goldenchain':'Goldenchain', 'Goldenraintree':'Goldenraintree', 'Hackberry':'Hackberry', 'Hackberry-Common':'Hackberry-Common', 'Hackberry-Netleaf':'Hackberry-Netleaf', 'Hawthorn':'Hawthorn', 'Hawthorn-Black':'Hawthorn-Black', 'Hawthorn-Cockspur':'Hawthorn-Cockspur', 'Hawthorn-English':'Hawthorn-English', 'Hawthorn-Lavalle':'Hawthorn-Lavalle', 'Hawthorn-Washington':'Hawthorn-Washington', 'Hickory':'Hickory', 'Honeylocust':'Honeylocust', 'Hornbeam':'Hornbeam', 'Horsechestnut-European':'Horsechestnut-European', 'Horsechestnut-Red':'Horsechestnut-Red', 'Ironwood':'Ironwood', 'Jujube':'Jujube', 'Juniper':'Juniper', 'Juniper-RockyMountain':'Juniper-RockyMountain', 'Larch':'Larch', 'Linden':'Linden', 'Linden-American':'Linden-American', 'Linden-Littleleaf':'Linden-Littleleaf', 'Linden-Silver':'Linden-Silver', 'Locust':'Locust', 'Locust-Black':'Locust-Black', 'Magnolia':'Magnolia', 'Maple-Amur':'Maple-Amur', 'Maple-Autumn':'Maple-Autumn', 'Maple-Bigtooth':'Maple-Bigtooth', 'Maple-Freeman':'Maple-Freeman', 'Maple-Hedge':'Maple-Hedge', 'Maple-Hybrid':'Maple-Hybrid', 'Maple-Japanese':'Maple-Japanese', 'Maple-Norway':'Maple-Norway', 'Maple-Paperback':'Maple-Paperback', 'Maple-Red':'Maple-Red', 'Maple-RockyMountain':'Maple-RockyMountain', 'Maple-Shantung':'Maple-Shantung', 'Maple-Silver':'Maple-Silver', 'Maple-StateStreet':'Maple-StateStreet', 'Maple-Sycamore':'Maple-Sycamore', 'Maple-Tatarian':'Maple-Tatarian', 'Mimosa':'Mimosa', 'Mohagany-CurleafMountain':'Mohagany-CurleafMountain', 'Mohagany-Mountain':'Mohagany-Mountain', 'Mulberry':'Mulberry', 'Mulberry-Red':'Mulberry-Red', 'Mulberry-White':'Mulberry-White', 'Oak':'Oak', 'Oak-Bur':'Oak-Bur', 'Oak-Chinkapin':'Oak-Chinkapin', 'Oak-English':'Oak-English', 'Oak-Gambel':'Oak-Gambel', 'Oak-Live':'Oak-Live', 'Oak-NorthernRed':'Oak-NorthernRed', 'Oak-SwampWhite':'Oak-SwampWhite', 'Orange-Osage':'Orange-Osage', 'Pagodatree-Japanese':'Pagodatree-Japanese', 'Pawpaw':'Pawpaw', 'Peach':'Peach', 'Pear-Common':'Pear-Common', 'Pear-Flowering':'Pear-Flowering', 'Pine':'Pine', 'Pine-Aleppo':'Pine-Aleppo', 'Pine-Austrian':'Pine-Austrian', 'Pine-Bosnian':'Pine-Bosnian', 'Pine-Bristlecone':'Pine-Bristlecone', 'Pine-Lodgepole':'Pine-Lodgepole', 'Pine-Himalayan':'Pine-Himalayan', 'Pine-JapaneseBlack':'Pine-JapaneseBlack', 'Pine-Lacebark':'Pine-Lacebark', 'Pine-Limber':'Pine-Limber', 'Pine-Mugo':'Pine-Mugo', 'Pine-Pinyon':'Pine-Pinyon', 'Pine-Ponderosa':'Pine-Ponderosa', 'Pine-Scotch':'Pine-Scotch', 'Pine-White':'Pine-White', 'Pistache-Chinese':'Pistache-Chinese', 'Pistachio':'Pistachio', 'Planetree-American':'Planetree-American', 'Planetree-London':'Planetree-London', 'Plum':'Plum', 'Plum-Common':'Plum-Common', 'Plum-Flowering':'Plum-Flowering', 'Plum-Purpleleaf':'Plum-Purpleleaf', 'Poplar':'Poplar', 'Poplar-Hybrid':'Poplar-Hybrid', 'Poplar-Lombardy':'Poplar-Lombardy', 'Poplar-White':'Poplar-White', 'Privet-Japanese':'Privet-Japanese', 'Quince':'Quince', 'Redbud':'Redbud', 'Redbud-Eastern':'Redbud-Eastern', 'Redbud-Western':'Redbud-Western', 'Redwood-Dawn':'Redwood-Dawn', 'RussianOlive':'RussianOlive', 'Serviceberry':'Serviceberry', 'Smoketree':'Smoketree', 'Spruce':'Spruce', 'Spruce-Blue':'Spruce-Blue', 'Spruce-Engelmann':'Spruce-Engelmann', 'Spruce-Norway':'Spruce-Norway', 'Spruce-Serbian':'Spruce-Serbian', 'Sweetgum-American':'Sweetgum-American', 'Tree-of-Heaven':'Tree-of-Heaven', 'TreeLilac':'TreeLilac', 'TreeLilac-Japanese':'TreeLilac-Japanese', 'Tuliptree':'Tuliptree', 'UNKNOWN':'UNKNOWN', 'Walnut-Black':'Walnut-Black', 'Walnut-English':'Walnut-English', 'Willow':'Willow', 'Willow-Narrowleaf':'Willow-Narrowleaf', 'Willow-Navajo/Globe':'Willow-Navajo/Globe', 'Willow-Peachleaf':'Willow-Peachleaf', 'Willow-Weeping':'Willow-Weeping', 'Yellowwood-Kentucky':'Yellowwood-Kentucky', 'Zelkova-Japanese':'Zelkova-Japanese'}
yesnoDict = {'Yes':'Yes', 'No':'No'}

# Add domain codes from the dictionaries with for loops
for code in cnt100by10Dict:
    arcpy.AddCodedValueToDomain_management(gdb_name, '100by10', code, cnt100by10Dict[code])
for code in cnt100by5Dict:
    arcpy.AddCodedValueToDomain_management(gdb_name, '100by5', code, cnt100by5Dict[code])
for code in cnt4by1Dict:
    arcpy.AddCodedValueToDomain_management(gdb_name, '4by1', code, cnt4by1Dict[code])
for code in cnt8by1Dict:
    arcpy.AddCodedValueToDomain_management(gdb_name, '8by1', code, cnt8by1Dict[code])
for code in commDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'comments', code, commDict[code])
for code in condDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'condition', code, condDict[code])
for code in mainDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'maintenance', code, mainDict[code])
for code in prioDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'priority', code, prioDict[code])
for code in scienDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'scientific', code, scienDict[code])
for code in specDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'species', code, specDict[code])
for code in yesnoDict:
    arcpy.AddCodedValueToDomain_management(gdb_name, 'yesno', code, yesnoDict[code])
    
    
#### 6. Create feature class
    
# Create path for geodatabase and feature class
gdb_path = os.path.join(data_folder, gdb_name)
fc_path = os.path.join(gdb_path, fc_name)

# Create spatial reference for feature class from wkid (4326 = GCS_WGS_1984)
srs = arcpy.SpatialReference(4326)

# Create feature class
arcpy.CreateFeatureclass_management(gdb_path, fc_name, geometry_type='Point', spatial_reference=srs)

# Create alias for feature class
arcpy.AlterAliasName(fc_path, alias_name)


#### 7. Add fields

# Create a list of tuples defining the fields
fields = [("TREE_ID", "TEXT", "", "", 20, "Tree ID", "NULLABLE", "NON_REQUIRED", ""),
("SPECIES", "TEXT", "", "", 50, "Species", "NON_NULLABLE", "REQUIRED", "species"),
("SCIENTIFIC_NAME", "TEXT", "", "", 50, "Scientific Name", "NULLABLE", "NON_REQUIRED", "scientific"),
("SPECIES_RATING", "SHORT", "", "", "", "Species Rating", "NULLABLE", "NON_REQUIRED", ""),
("DIAMETER", "FLOAT", "", "", "", "Diameter", "NON_NULLABLE", "NON_REQUIRED", "100by1"),
("HEIGHT", "SHORT", "", "", "", "Height", "NON_NULLABLE", "NON_REQUIRED", "100by5"),
("SPREAD", "SHORT", "", "", "", "Spread", "NON_NULLABLE", "NON_REQUIRED", "100by5"),
("TREE_CONDITION", "TEXT", "", "", 20, "Tree Condition", "NULLABLE", "REQUIRED", "condition"),
("ROOT_HEALTH", "SHORT", "", "", "", "Root Health", "NON_NULLABLE", "NON_REQUIRED", "8by1"),
("TRUNK_HEALTH", "SHORT", "", "", "", "Trunk Health", "NON_NULLABLE", "NON_REQUIRED", "8by1"),
("SCAFFOLD_BRANCHES", "SHORT", "", "", "", "Scaffold Branches", "NON_NULLABLE", "NON_REQUIRED", "8by1"),
("SMALL_BRANCHES", "SHORT", "", "", "", "Small Branches", "NON_NULLABLE", "NON_REQUIRED", "4by1"),
("FOLIAGE_HEALTH", "SHORT", "", "", "", "Foliage Health", "NON_NULLABLE", "NON_REQUIRED", "4by1"),
("CONDITION_RATING", "DOUBLE", "", "", "", "Condition Rating", "NULLABLE", "NON_REQUIRED", ""),
("SITE_RATING", "SHORT", "", "", "", "Site Rating", "NON_NULLABLE", "NON_REQUIRED", "100by10"),
("SITE_CONTRIBUTION", "SHORT", "", "", "", "Site Contribution", "NON_NULLABLE", "NON_REQUIRED", "100by10"),
("PLACEMENT_RATING", "SHORT", "", "", "", "Placement Rating", "NON_NULLABLE", "NON_REQUIRED", "100by10"),
("LOCATION_RATING", "DOUBLE", "", "", "", "Location Rating", "NULLABLE", "NON_REQUIRED", ""),
("APPRAISED_VALUE", "DOUBLE", "", "", "", "Appraised Value",  "NULLABLE", "NON_REQUIRED", ""),
("MAINTENANCE_PRIORITY", "TEXT", "", "", 40, "Maintenance Priority", "NULLABLE", "NON_REQUIRED", "priority"),
("MAINTENANCE1", "TEXT", "", "", 40, "Maintenance", "NULLABLE", "NON_REQUIRED", "maintenance"),
("MAINTENANCE2", "TEXT", "", "", 40, "Maintenance 2", "NULLABLE", "NON_REQUIRED", "maintenance"),
("COMMENTS1", "TEXT", "", "", 40, "Comments", "NULLABLE", "NON_REQUIRED", "comments"),
("COMMENTS2", "TEXT", "", "", 40, "Comments 2", "NULLABLE", "NON_REQUIRED", "comments"),
("FLAGSHIP", "TEXT", "", "", 10, "Flagship", "NULLABLE", "NON_REQUIRED", "yesno"),
("YEAR", "SHORT", "", "", "", "Year", "NULLABLE", "NON_REQUIRED", ""),
("NOTES", "TEXT", "", "", 100, "Notes", "NULLABLE", "NON_REQUIRED", "")]

# Add the fields using a for loop
for field in fields:
    arcpy.AddField_management(fc_path, field[0], field[1], field[2], field[3], field[4], field[5], field[6], field[7], field[8])

# Assign a default value of 0 to integer fields for later calculations
arcpy.AssignDefaultToField_management(fc_path, 'DIAMETER', 0)
arcpy.AssignDefaultToField_management(fc_path, 'ROOT_HEALTH', 0)
arcpy.AssignDefaultToField_management(fc_path, 'TRUNK_HEALTH', 0)
arcpy.AssignDefaultToField_management(fc_path, 'SCAFFOLD_BRANCHES', 0)
arcpy.AssignDefaultToField_management(fc_path, 'SMALL_BRANCHES', 0)
arcpy.AssignDefaultToField_management(fc_path, 'FOLIAGE_HEALTH', 0)
arcpy.AssignDefaultToField_management(fc_path, 'SITE_RATING', 0)
arcpy.AssignDefaultToField_management(fc_path, 'SITE_CONTRIBUTION', 0)
arcpy.AssignDefaultToField_management(fc_path, 'PLACEMENT_RATING', 0)


#### 8. Feature class and geodatabase created

arcpy.AddMessage('Feature class and geodatabase created')











