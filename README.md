# Tree Inventory Final Project

## Description

For my project, I chose to create a tool which can be used to assist in collecting tree inventories. The goal was to create a script that could quickly and consistently create a feature class with a set schema that could be used with the ArcGIS Collector app to collect locations of urban trees.

## Process

#### The tool consists of a Toolbox with three separate script tools:<br>
1. The first is to be used when starting fresh with no existing inventory. This script creates a geodatabase, adds range and coded domains, creates a feature class, adds required fields, and finally adds the domains and any default values to those fields<br>
2. The second is to be used where there is an existing inventory but is missing certain fields for calculations. This script points to the existing inventory feature class and adds the fields for species, condition, and location ratings as well as the appraised value.<br>
3. The third is to be used when there is an existing inventory and all required fields. This script sets the species rating based off of the tree species, includes variables for installation costs that may vary between inventories, and finally calculates the values that ultimately feed into a trees final appraisal value in dollars.

![title](images/tbx.png)

## Deliverables

#### Included in this project repository are:<br>
1. The toolbox "Tree_Inventory_Tools.tbx" along with the scripts to run.<br>
2. A geodatabase "Demo_Tree_Inventory.gdb" with layers to use in a demonstration of the tools.<br>
3. An excel file listing all of the domains, the equation that produces the appraisal cost, and the species ratings codes.

![title](images/data.png)
