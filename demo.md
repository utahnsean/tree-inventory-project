# Demo

This document will run through a demonstration of the tools included in this project. All data for this demo is included in the project repository

## Create a geodatabase and feature class

This script will ask the user to input four variables/parameters: <br>
1. Select a folder to house the data
2. Create a name for the geodatabase
3. Create a name for the feature class
4. Create an alias for the feature class that will display in ArcMap

![title](images/input1.PNG)

The script may take a minute or two depending on the speed of the processor. In the background, the script is creating 12 domains, both ranged and coded, and adding those domains to a feature class. Included in the domains are 183 tree names, both common and scientific names. After setting up the domains, the script adds 27 fields to the feature class and assigns the domains where necessary.

![title](images/domains1.PNG)

![title](images/fields1.PNG)

If all goes well, the process should complete with the message "Feature class and geodatabase created".

![title](images/complete1.PNG)

There should now be a folder with geodatabase and one feature class. This feature class is ready to be added to a webmap and with the ArcGIS Collector app capture locations of individual trees.

![title](images/demo1.PNG)

## Add Fields to Existing Feature Class

This script is to be used where there is an existing inventory feature class that is missing certain fields. The user will select an existing feature class when run and the script will add the fields only the fields Species Rating, Condition Rating, Location Rating, and Appraised Value.

Included in the "Demo_Tree_Inventory.gdb" is the feature class "demo_vineyard_walsh_tree_inventory" that is set up for this demo. The Walsh inventory is an existing tree inventory of lots being developed, however, when the inventory was collected, no ratings or calculations were collected. This is the perfect instance to add those fields here.

![title](images/walsh1.PNG)

Open the tool, navigate to the Walsh inventory, and hit OK to run.

![title](images/walsh2.PNG)

This tool should process very quickly. Once it has completed, add the Walsh inventory layer to ArcMap if it is not already open, open the attribute table and scroll to the far right. There should be four new field columns with <Null> values if everything ran correctly.

![title](images/walsh3.PNG)

## Set Species Rating and Calculate Appraised Value

This script is to be used when there is an existing tree inventory with all of the correct fields but no calculations made. This script will ask the user to input four variables/parameters. They are:<br>
1. Select an existing inventory feature class to hold the calculations
2. Input replacement tree size, a numerical value
3. Input installed tree cost, a numerical value
4. Input unit tree cost, a numerical value

Included in "Demo_Tree_Inventory.gdb" is the feature class "demo_vineyard_grove_park_tree_inventory". Navigate to that file for the first parameter in the tool. The remaining three parameters have the high likelihood to vary depending on the location of the tree inventory being conducted. In the example, it was determined by arborists that the minimum tree size was 2.4 feet, the cost of planting a tree to be 413 dollars, and the cost per tree 58 dollars. Enter those values in the spaces provided.

![title](images/calc1.PNG)

In the background, the script is using an update cursor to look at the species name and assign a numerical rating depending on that species. With those species ratings set, the script is then taking the three numerical parameters entered, and along with the diameter and eight of the other numerical fields, and calculating ratings for the condition and location and ultimately the appraisal value in dollars. Included in the project repository is an excel file with the calculation used and an example

![title](images/cursor1.PNG)

![title](images/cursor2.PNG)

The script should run fairly quickly. Opening the attribute table of the Grove Park inventory you can explore the final calculations of the ratings and appraisal values.

![title](images/calc2.PNG)

With the appraisal value calculated, this shapefile can now be used in anything from descriptive maps to dashboards.


```python

```
