# Project Proposal

For my project, I want to help my friend’s tree care company with a script that can automate some of the work they do after an inventory is conducted. The tree care company, Treewise, conducts inventories of a community’s urban trees. With Collector for ArcGIS, arborists in the field can gather key attributes for trees they encounter, that includes height, diameter, spread, species, location, and maintenance value. After an inventory, they are left with a point shapefile to produce maps with and can export that data to an excel file to calculate certain benefits from the trees. These benefits include installation cost, replacement cost, unit cost, appraised trunk increase, and tree cost and are calculated with industry-accepted formulas.

I think it would be helpful to put together a toolbox with a script that takes an existing shapefile as a parameter and goes through the steps of adding new fields and calculating those values. Currently, those calculations are done in excel, and either kept separate from the GIS or appended to the shapefile later. This project would benefit the company through a single streamlined process and removing the “disconnecting” from GIS that occurs to perform the calculations.

## Steps

I see this project involving the creation of a script and toolbox, within the script using a cursor to add fields and perform data calculations when there is an existing shapefile to work with. There will also be the possibility of creating a new shapefile with fields and domains that could be used for new inventories. I also want to add some code that produces several maps; these would include creating queries to pull out trees that need to be replaced, trees that are in great shape, trees that have a high appraisal value, etc.

I think it might be a bit light on workload and complexity, and I am eager to see what you think.
